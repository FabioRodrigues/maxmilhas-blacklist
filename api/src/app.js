'use strict';

const Hapi = require('hapi');  
const utils = require('./services/utils/utils.js');    


async function start(){
    try{
        const server = new Hapi.Server({  
            port: process.env.SERVER_PORT || 3000,
            host: '0.0.0.0',
            routes: {
                cors: true
            }
          });

          await utils.addRoute(server);
          

          await server.start();
          console.log('running at ' + server.info.uri);
    }
    catch(err){
        console.log(err);
        process.exit(0);
    }
}

start()