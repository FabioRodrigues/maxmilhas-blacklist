'use strict'

const chai = require('chai')
chai.use(require('sinon-chai'))
const expect = chai.expect
const controller = require('../../../controllers/blacklist.controller')
const service = require('../../../services/blacklist.service')
const sandbox = require('sinon').createSandbox();
const Boom = require('boom');

const cpf = '94336716609';

const responseHandlerCustom = {
  response: (body) => {
    return {
      code: (c) => {
        return c;
      }
    }
  }
};

const responseHandlerCustomBody = {
  response: (body) => {
    return {
      code: (c) => {
        return body
      }
    }
  }
};



describe('Controller blacklist', () => {
  afterEach(function () { sandbox.restore(); });
  beforeEach(function () {
    sandbox.stub(Boom, 'badRequest').callsFake(() => { return 400 });
    sandbox.stub(Boom, 'internal').callsFake(() => { return 500 });
  });

  it('deve retornar CREATED caso CPF seja inserido na blacklist', () => {
    sandbox.stub(service, 'create').callsFake(() => { return true; });

    var result = controller.create({ payload: { cpf: cpf } }, responseHandlerCustom);
    expect(result).is.equals(201);
  })

  it('deve retornar BAD REQUEST caso CPF seja invalido ao tentar inserir', () => {
    sandbox.stub(service, 'create').callsFake(() => { return true; });

    var result = controller.create({ payload: { cpf: cpf + "11" } }, responseHandlerCustom);
    expect(result).is.equals(400);
  })

  it('deve retornar INTERNAL SERVER ERROR caso nao seja possivel inserir cpf na blacklist', () => {
    sandbox.stub(service, 'create').callsFake(() => { return false; });

    var result = controller.create({ payload: { cpf: cpf } }, responseHandlerCustom);
    expect(result).is.equals(500);
  })


  it('deve retornar INTERNAL SERVER ERROR caso aconteca algum erro nao tratado ao inserir CPF na blacklist', () => {
    sandbox.stub(service, 'create').throws("Erro");

    var result = controller.create({ payload: { cpf: cpf } }, responseHandlerCustom);
    expect(result).is.equals(500);
  })

  it('deve retornar OK ao remover CPF da blacklist com sucesso', () => {
    sandbox.stub(service, 'remove').callsFake(() => { return true; });

    var result = controller.remove({ params: { cpf: cpf } }, responseHandlerCustom);
    expect(result).is.equals(200);
  })

  it('deve retornar BAD REQUEST caso CPF seja invalido ao tentar remover', () => {
    sandbox.stub(service, 'remove').callsFake(() => { return true; });

    var result = controller.remove({ params: { cpf: cpf + "11" } }, responseHandlerCustom);
    expect(result).is.equals(400);
  })

  it('deve retornar INTERNAL SERVER ERROR caso nao seja possivel remover cpf da blacklist', () => {
    sandbox.stub(service, 'remove').callsFake(() => { return false; });

    var result = controller.remove({ params: { cpf: cpf } }, responseHandlerCustom);
    expect(result).is.equals(500);
  })


  it('deve retornar INTERNAL SERVER ERROR caso exista erro nao tratado ao remover cpf da blacklist', () =>  {
    sandbox.stub(service, 'remove').throws("Erro");

    var result = controller.remove({ params: { cpf: cpf } }, responseHandlerCustom);
    expect(result).is.equals(500);
  })

  it('deve retornar status FREE na caso CPF nao esteja na blacklist', () =>  {
    sandbox.stub(service, 'findDocumentStatus').callsFake(() => { return "FREE"; });

    var result = controller.findByCpf({ params: { cpf: cpf } }, responseHandlerCustomBody);
    console.log(result);

    expect(result.status).is.equals("FREE");
  })

  it('deve retornar status BLOCK na caso CPF esteja na blacklist', () =>  {
    sandbox.stub(service, 'findDocumentStatus').callsFake(() => { return "BLOCK"; });

    var result = controller.findByCpf({ params: { cpf: cpf } }, responseHandlerCustomBody);
    expect(result.status).is.equals("BLOCK");
  })

  it('deve retornar BAD REQUEST caso CPF informado seja inválido', () =>  {
    sandbox.stub(service, 'findDocumentStatus').callsFake(() => { return "FREE"; });

    var result = controller.findByCpf({ params: { cpf: cpf + "11" } }, responseHandlerCustom);
    expect(result).is.equals(400);
  })

  it('deve retornar INTERNAL SERVER ERROR caso encontre erro ao processar busca', () =>  {
    sandbox.stub(service, 'findDocumentStatus').throws("Erro");

    var result = controller.findByCpf({ params: { cpf: cpf } }, responseHandlerCustom);
    expect(result).is.equals(500);
  })
})

