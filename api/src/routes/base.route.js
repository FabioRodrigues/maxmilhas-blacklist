'use strict';
const Joi = require('joi');
const Boom = require('boom');
const utils = require('../services/utils/utils');

module.exports = [{
    method: 'GET',
    path: '/healthcheck',
    handler: (request, h) => {
        return h.response({
            status: "alive",
            requestsReceived: utils.getTotalRequests()
        }).code(201);
    },
    options: {
        auth: false,
        validate: {
            options: {
                abortEarly: false,
                allowUnknown: true
            }
        },
        description: 'Obtém status da aplicação',
        notes: 'Retorna se a API está viva',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '200': {
                        'description': 'Api respondendo'
                    }
                },
                
            }
        }
    }
},
{
    method: '*',
    path: '/{p*}',
    handler: (request, h) => {
        return Boom.badRequest('route does not exist');
    },
    options: {
        auth: false,
        validate: {
            options: {
                abortEarly: false,
                allowUnknown: true
            }
        }
    }
}]