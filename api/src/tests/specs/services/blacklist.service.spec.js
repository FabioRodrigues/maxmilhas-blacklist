'use strict'

const chai = require('chai')
chai.use(require('sinon-chai'))
const expect = chai.expect
const service = require('../../../services/blacklist.service')
const db = require('../../../services/utils/database')
let sandbox = require('sinon').createSandbox();
const sinon = require('sinon')

const cpf = '94336716609';
const cpfComPontuacao = '943.367.166-09';

describe('Servico de blacklist', function () {
  afterEach(function () { sandbox.restore(); });

  it('deve retornar verdadeiro caso consiga inserir um novo cpf na blacklist', () => {
    sandbox.stub(db, 'find').callsFake(() => { return []; })
    let stubCreate = sandbox.stub(db, 'create');
    
    let result = service.create(cpf)

    expect(result).to.be.true;
    sinon.assert.calledOnce(stubCreate)
  })

  

  it('deve retornar FREE caso cpf nao exista no sistema', () => {
    sandbox.stub(db, 'find').callsFake(() => { return []; })

    let result = service.findDocumentStatus(cpf)
    expect(result).is.equals("FREE");
  })

  it('deve retornar BLOCK caso cpf exista no sistema', () => {
    sandbox.stub(db, 'find').callsFake(() => { return [{cpf: cpf}]; })

    let result = service.findDocumentStatus(cpf)
    expect(result).is.equals("BLOCK");
  })

  it('deve remover o CPF da blacklist', () => {
    sandbox.stub(db, 'remove');

    let result = service.remove(cpf)
    expect(result).to.be.true;
  })


  it('deve inserir cpf sem pontuacao na blacklist', () => {
    sandbox.stub(db, 'find').callsFake(() => { return []; })
    let stubCreate = sandbox.stub(db, 'create');

    service.create(cpfComPontuacao)
    expect(stubCreate).to.be.calledWith('blacklist', {cpf: cpf});
  })

  it('deve buscar cpf sem pontuacao na blacklist', () => {
    let stubFind = sandbox.stub(db, 'find').callsFake(() => { return []; })

    service.findDocumentStatus(cpfComPontuacao)
    expect(stubFind).to.be.calledWith('blacklist', {cpf: cpf});
  })

  it('deve remover cpf sem pontuacao na blacklist', () => {
    let stubRemove = sandbox.stub(db, 'remove').callsFake(() => { return []; })

    service.remove(cpfComPontuacao)
    expect(stubRemove).to.be.calledWith('blacklist', {cpf: cpf});
  })
 
})

