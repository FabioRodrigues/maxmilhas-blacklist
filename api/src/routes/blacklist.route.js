'use strict';
const Joi = require('joi')
const Boom = require('boom');
const BacklistController = require('../controllers/blacklist.controller')


module.exports = [{
    method: 'GET',
    path: '/blacklist/cpf/{cpf}',
    handler: BacklistController.findByCpf,
    options: {
        validate: {
            params: {
                cpf: Joi.required()
            },
            options: {
                abortEarly: false,
                allowUnknown: true
            }
        },
        description: 'Obtém a situação do CPF',
        notes: 'Retorna se o CPF está na blacklist',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '200': {
                        'description': 'Sucesso na buscao'
                    },
                    '400':{
                        'description': 'CPF inválido'
                    },
                    '500':{
                        'description': 'Erro interno'
                    }
                },
                
            }
        },   
        auth: false
    }
},
{
    method: 'POST',
    path: '/blacklist',
    handler: BacklistController.create,
    options: {
        validate: {
            payload: {
                cpf: Joi.string().required()
            },
            options: {
                abortEarly: false,
                allowUnknown: true
            }
        },
        plugins: {
            'hapi-swagger': {
                responses: {
                    '201': {
                        'description': 'CPF incluido na blacklist'
                    },
                    '400':{
                        'description': 'CPF inválido'
                    },
                    '500':{
                        'description': 'Erro interno'
                    }
                },
                
            }
        },        
        description: 'Insere um novo CPF na blacklist',
        notes: 'Adiciona o CPF na blacklist',
        tags: ['api'], 
        auth: false
    }
},
{
    method: 'DELETE',
    path: '/blacklist/{cpf}',
    handler: BacklistController.remove,
    options: {
        validate: {
            params: {
                cpf: Joi.required()
            },
            options: {
                abortEarly: false,
                allowUnknown: true
            }
        },
        description: 'Remove o CPF da blacklist',
        notes: 'Caso o CPF esteja na blacklist, o mesmo é removido',
        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                responses: {
                    '200': {
                        'description': 'Sucesso na remocao do CPF da blacklist'
                    },
                    '400':{
                        'description': 'CPF inválido'
                    },
                    '500':{
                        'description': 'Erro interno'
                    }
                },
                
            }
        }, 
        auth: false
    }
}
]