# Projeto Blacklist

Este projeto se propõe a criar um controle de pessoas que estão ou não em uma lista restrita. O mesmo é feito utilizando o CPF.


### Estrutura do projeto:

```
root
│   README.md  //Este é o arquivo que você está lendo
│   docker-compose.yml  //Arquivo para subir e orquestrar os containers    
│
└─── api
│    │ Dockerfile // arquivo de configuracao do container da API
│    │ .dockerignore //arquivo para evitar arquivos indesejados dentro do container
│    │   
│    └─── src
│         │ app.js
│         │ package.json
│         └─── controllers //controllers do sistema
│         │ 
│         └─── routes //configuracao das rotas
│         │ 
│         └─── services // services do sistema
│         │ 
│         └─── tests //specs para testes (unitarios)
│         │ 
│         └─── coverage //resultado da cobertura em formato HTML
│ 
│ 
└─── web
│     │ Dockerfile // arquivo de configuracao do container web
│     │ .dockerignore //arquivo para evitar arquivos indesejados dentro do container
│     │ babel.config.js
│     │ package.json
│     │ 
│     └─── public // arquivos iniciais do vue.js
│     │ 
│     └─── src
│         │ App.vue // arquivo base do app
│         │ main.js // arquivo base do app
│         │ router.js // arquivo de rotas
│         │ 
│         └─── components //componentes do app
│         │ 
│         └─── assets //assets do app
│         │ 
│         └─── views // views do app
│
│
│
```


## Instalação

É necessário instalar o [docker](https://www.docker.com/) e o [docker-compose](https://docs.docker.com/compose/) para iniciar o processo de instalação.

Após instalados os componentes acima, na raiz do projeto, executar o comando:
```bash
docker-compose up
```

## Utilização
#### Após todo o processo de subida dos containers:

- WEB

    Acesse o endereço *http://localhost*

- API

    **Documentação das rotas em:** *http://localhost:3000/documentation* **(swagger)**
    
    Para consultar o status da API, acesse o endereço *http://localhost:3000/healthcheck*


# Detalhes
### API
O projeto foi construindo utilizando principalmente:
- [hapi.js](https://hapijs.com/) Framework para criacao de um servidor web com gerenciamento de rotas, recursos e todo o ecossistema
- [lowdb](https://github.com/typicode/lowdb) Framework de banco de dados local baseado em JSON  provido pela equipe do [Lodash](https://lodash.com/)
- [mocha](https://mochajs.org/) Framework para execução de testes
- [sinon.js](https://sinonjs.org/) Framework para mocks e stubs de objetos

### Comandos

```bash
npm run start # inicia a api
npm run test # executa os testes unitários da api 
npm run pretest # executa um linter
npm run coverage # executa os testes e exibe a cobertura dos testes
npm run report # gera um relatório da cobertura de testes em formato html na pasta /coverage

```
- Testes:

![unittests](https://i.imgur.com/WdF5yk6.png)

- Cobertura:
![coverage](https://i.imgur.com/wivaiXv.png)

### WEB
O Projeto web foi criado utilizando principalmente 

- [Vue.js](https://vuejs.org/) Framework para a construção de páginas web reativas, com ferramentas para minificação, build etc.
- [bulma.io](https://bulma.io/) Framework css baseado em [Flexbox](https://developer.mozilla.org/pt-BR/docs/Learn/CSS/CSS_layout/Flexbox)

### Comandos:

```bash
npm run serve #inicia a aplicação em modo de desenvolvimento
npm run build #compila o projeto em modo produção e publica na pasta /dist
```

