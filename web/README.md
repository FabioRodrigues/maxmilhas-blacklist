# Maxmilhas - buscador

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build

see it at '/dist'
```
### Lints and fixes files
```
npm run lint
```

## Container sample

Build an image:
```
docker build -t maxmilhas-site-0.0.1 .

```

Run a container based on your just-created image:

```
docker run --name maxmilhas -p 80:80 -d  maxmilhas-site-0.0.1 

```

Explore: http://localhost:80 (or just localhost)