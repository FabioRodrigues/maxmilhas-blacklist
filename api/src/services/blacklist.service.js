'use strict'

const cpfvalidator = require("@fnando/cpf/dist/node");
const db = require('../services/utils/database');

module.exports =
    {
        create: (cpf) => {
            let body = { cpf: cpfvalidator.strip(cpf) };

            if (db.find('blacklist', body).length == 0)
                db.create('blacklist', body);

            return true;
        },
        findDocumentStatus: (cpf) => {
            let body = { cpf: cpfvalidator.strip(cpf) };
            return db.find('blacklist', body)
            .length == 0 
            ? "FREE"
            : "BLOCK";
        },
        remove: (cpf) => {
            let body = { cpf: cpfvalidator.strip(cpf) };
                db.remove('blacklist', body);

            return true;
        }
    };
