'use strict';

const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db.json')
const db = low(adapter)

db.defaults({ blacklist: [] })
  .write()

module.exports={
    find: (obj, query) => {
        return db
        .get(obj)
        .filter(query)
        .value();
    },
    create: (obj, body) => {
        db
        .get(obj)
        .push(body)
        .write();
    },
    remove: (obj, body) => {
        db
        .get(obj)
        .remove(body)
        .write();
    }
}