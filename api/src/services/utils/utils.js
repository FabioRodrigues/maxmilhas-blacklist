'use strict';
const path = require('path');
const Path = require('path');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('../../package');

const swaggerOptions = {
    info: {
        title: 'API blacklist Documentation',
        version: Pack.version,
    },
};

let getFiles = (path) => {
    path = path[path.length - 1] !== '/' ? path + '/' : path;
    let files = [];
    try {
        files = require('fs').readdirSync(Path.resolve(__dirname, '../..', path));
    } catch (e) {
        console.log(e);
        process.exit();
    }
    return files.map((file) => {
        return Path.resolve(__dirname, '../..', path, file)
    });
}

let totalRequests = 0;
module.exports = {
    getTotalRequests: () => {
        return totalRequests;
    },
    addRoute: async (server) => {
        getFiles('routes').forEach((routesFile) => {

            require(routesFile).forEach((route) => {
                server.route(route);
            });
        });

        await server.register([
            Inert,
            Vision,
            {
                plugin: HapiSwagger,
                options: swaggerOptions
            }
        ]);

        server.ext({
            type: 'onRequest',
            method: function (request, h) {
                totalRequests++;
                return h.continue;
            }
        });

    },
    getLinksBlackList: (request, additionalLinks, cpf) => {
        if (!request.url)
            return [];

        let baseUrl = request.url.origin;
        let paths = [];
        paths.push({
            "action": request.method.toUpperCase(),
            "rel": "self",
            "href": request.url.href
        })

        additionalLinks.forEach(ad => {
            switch (ad) {
                case "remove": {
                    paths.push(
                        {
                            'action': 'DELETE',
                            'rel': 'remove',
                            'href': `${baseUrl}/blacklist/${cpf}`
                        })
                    break;
                }
                case "insert": {
                    paths.push(
                        {
                            'action': 'POST',
                            'rel': 'insert',
                            'href': `${baseUrl}/blacklist`
                        })
                    break;
                }
                case "search": {
                    paths.push(
                        {
                            'action': 'GET',
                            'rel': 'find',
                            'href': `${baseUrl}/blacklist/cpf/${cpf}`
                        })
                    break;
                }
                default:
                    break;
            }

        });

        return paths;
    }

};