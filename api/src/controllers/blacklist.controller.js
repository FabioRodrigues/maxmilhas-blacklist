'use strict';
const Boom = require('boom');
const cpf = require("@fnando/cpf/dist/node");
const service = require('../services/blacklist.service')
const utils = require('../services/utils/utils')


module.exports = {
    create: (request, h) => {
        try {
            let document = cpf.strip(request.payload.cpf);

            if (!cpf.isValid(document))
                return Boom.badRequest("Cpf invalido");

            if (service.create(document))
                return h.response({ 
                        cpf: cpf.strip(request.payload.cpf), 
                        links: utils.getLinksBlackList(request, ['search', 'remove'], document) 
                    }).code(201);

            return Boom.internal("erro ao criar");

        } catch (err) {
            return Boom.internal(err);
        }
    },
    remove: (request, h) => {
        try {
            let document = cpf.strip(request.params.cpf);

            if (!cpf.isValid(document))
                return Boom.badRequest("Cpf invalido");

            if(service.remove(document))
                return h.response({
                    links: utils.getLinksBlackList(request, ['search', 'insert'], document)
                }).code(200);

            return Boom.internal();

        } catch (err) {
            return Boom.internal(err);
        }
    },
    findByCpf: (request, h) => {
        try {
            
            let document = cpf.strip(request.params.cpf);

            if (!cpf.isValid(document))
                return Boom.badRequest("Cpf invalido");

            return h.response(
                {
                status: service.findDocumentStatus(document),
                links: utils.getLinksBlackList(request,['remove', 'insert'], document)
            }).code(200);

        } catch (err) {
            return Boom.internal(err);
        }
    }
}